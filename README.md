# Beetlsql

* 作者: 闲大赋,Gavin.King,Sue,Zhoupan
* 开发时间:2015-07
* 论坛 http://ibeetl.com
* qq群 219324263
* 当前版本 2.0 (150K), 另外还需要beetl 包
* 文档地址: http://ibeetl.com/guide/beetlsql.html 或者 https://git.oschina.net/xiandafu/beetlsql/attach_files 下载pdf 
 
# beetlsql 特点

BeetSql是一个全功能DAO工具， 同时具有Hibernate 优点 & Mybatis优点功能，适用于承认以SQL为中心，同时又需求工具能自动能生成大量常用的SQL的应用。

* 无需注解，自动使用大量内置SQL，轻易完成增删改查功能，节省50%的开发工作量
* 数据模型支持Pojo，也支持Map/List这种快速模型，也支持混合模型
* SQL 以更简洁的方式，Markdown方式集中管理，同时方便程序开发和数据库SQL调试。
* SQL 模板基于Beetl实现，更容易写和调试，以及扩展
* 简单支持关系映射而不引入复杂的OR Mapping概念和技术。
* 具备Interceptor功能，可以调试，性能诊断SQL，以及扩展其他功能
* 首个内置支持主从数据库支持的开源工具，通过扩展，可以支持更复杂的分库分表逻辑
* 支持跨数据库平台，开发者所需工作减少到最小，目前跨数据库支持mysql,postgres,oracle,sqlserver,h2,sqllite.
* 可以针对单个表(或者视图）代码生成pojo类和sql模版，甚至是整个数据库。能减少代码编写工作量




# Hibernate,MyBatis,MySQL 对比

http://ibeetl.com/community/?/article/63  提供了12项对比并给与评分。在犹豫使用BeetlSQL，可以参考这个全面的对比文章



# 开发人员帅照

<img src="http://ibeetl.com/guide/xiandafu.jpg" width = "100" height = "100"  />

<img src="http://ibeetl.com/guide/GV2.png" width = "100" height = "100"  />

<img src="http://ibeetl.com/guide/SUE.jpg" width = "100" height = "100"  />


<img src="http://ibeetl.com/guide/fitz.jpg.png" width = "100" height = "100"  />


